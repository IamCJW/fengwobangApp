apiready = function() {
    var photoBrowser = api.require('photoBrowser');
    var wxPlus = api.require('wxPlus');
    var clipBoard = api.require('clipBoard');
    var iOSDialog = api.require('iOSDialog');
    var app = new Vue({
        el: '#app',
        data: {
			iosTest:false,
            isShowTaskDetail: false,
            taskDateTimer: null,
            commitTimer: null,
            // 任务信息
            taskLink: {
                id: '',
                iconUrl: '',
                name: '',
                createTime: '',
                // 进度比例
                progressRatio: 0,
                // 单价
                price: 0,
                claim: '',
                auditTime: 0,
                limit: '',
                // 悬赏人头像
                pubImg: '',
                // 悬赏人名字
                pubName: '',
                // 悬赏人id
                uid: 0,
                diffculty_level: 0
            },
            // 接单状态
            acceptStatus: null,
            // 任务状态
            taskStatus: {
                tag: "Published",
                text: "进行中",
                value: null,
            },
            // 任务过期时间
            taskEndTime: {
                isEnd: true,
                spec: '已过期'
            },
            // 任务步骤
            taskSteps: [],
            targets: null,
            commands: null,
            // 是否登录
            isLogin: false,
            bid: null,
            // 当前上传文件属性
            curUploadLink: {
                type: '',
                arrItem: '',
                stepIndex: '',
                contIndex: '',
                arrIndex: ''
            },
            commitResidueText: '',
            // 任务id
            taskId: '',
            // 接单id
            acceptId: '',
            isPageLoading: true,
            taskWran: '',

            //判断是否绑定过微信号码
            is_bind_weixin:1,
            rewarde_money:"",
            time_last:0,
            time_lastText:"",
        },
        mounted(){
			$.initJs(this);
            this.init();
        },
        computed: {
             // 按钮状态 0 未领取  1 待提交 2 已提交 3 过期
            btnStatus() {
                var acceptStatus = this.acceptStatus;
                var taskStatus = this.taskStatus;
                if (!acceptStatus) {
                    return 0;
                }
                if (acceptStatus.value == 1) {
                    return 1;
                }
                if (acceptStatus.value == 2 || acceptStatus.value == 3 || acceptStatus.value == 4 || acceptStatus.value == 5) {
                    return 2;
                }
                if (acceptStatus.value == 6 && taskStatus.value == 2) {
                    return 0;
                }
                if (taskStatus.value == 5 || this.taskEndTime.isEnd) {
                    return 3;
                }
            },
            btnStyleShow() {
                // 按钮状态 0 未领取  1 待提交 2 已提交 3 过期
                var btnStatus = Number(this.btnStatus);
                // if (!this.loginStatus) {
                //     return '';
                // }
                if (btnStatus == 2) {
                    return '_already';
                }
                if (btnStatus == 3) {
                    return '_end';
                }
                return '';
            },
            btnText() {
                var acceptStatus = this.acceptStatus;
                var taskStatus = this.taskStatus;
                if (!this.isLogin) {
                    return '请先登录后操作';
                }
                if (!acceptStatus && taskStatus.value == 2) {
                    return '领取任务';
                }
                var statusId = acceptStatus && Number(acceptStatus.value);
                if (statusId === 1) {
                    return '提交任务';
                }
                if (statusId === 2) {
                    return '已提交任务, 待审核';
                }
                if (statusId === 3) {
                    return '已提交任务, 已通过';
                }
                if (statusId === 4) {
                    return '已提交任务, 仲裁中';
                }
                if (statusId === 5) {
                    return '已提交任务, 未通过';
                }
                if (this.btnStatus == 3) {
                    return '任务已过期'
                }
                return '领取任务';
            },
            showBtn() {
                let btnStatus = Number(this.btnStatus);
                if (this.getPageParam().data.acceptId && btnStatus !== 1) {
                    return false;
                }
                return true;
            },
            statusSpec() {
                let btnStatus = Number(this.btnStatus);
                if (btnStatus === 0) {
                    return '可接'
                }
                if (btnStatus === 1) {
                    return '待提交'
                }
                if (btnStatus === 2) {
                    var acceptStatus = this.acceptStatus;
                    var statusId = acceptStatus && Number(acceptStatus.value);
                    if (statusId === 2) {
                        return '待审核';
                    }
                    if (statusId === 3) {
                        return '已通过';
                    }
                    if (statusId === 4) {
                        return '仲裁中';
                    }
                    if (statusId === 5) {
                        return '未通过';
                    }
                }
                if (btnStatus === 3) {
                    return '不可接'
                }
            },
            isShowTextCache() {
                if (this.btnStatus != 1) {
                    return false;
                }
                var textList = this.handleTextInputCache('get');
                if (textList && textList[0]) {
                    return true;
                }
                return false;
            }
        },
        methods: {
            init: function (load) {
                var userToken = $.getUser() && $.getUser().authorized_key;
                var params = this.getPageParam().data;
                this.taskId = params.taskId;
                if (userToken) {
                    this.isLogin =  true;
                    this.bid = this.createId();
                }
                this.getOpercommandsApi(load);
            },
            /**
             *  生产bid
             */
            createId() {
                var bid = $.createBidFn(CONFIG.constants.TASK);
                return bid;
            },
            /*
            * 获取页面传递参数
            */
            getPageParam: function() {
                 //console.log(JSON.stringify(api.pageParam));
                return api.pageParam;
            },
            handleShowDetail: function() {
                this.isShowTaskDetail = !this.isShowTaskDetail;
            },
            getTaskDetailApi: function (load) {
                var _self = this;
                var resPar = {
                    task_id: this.taskId
                };
                if (this.getPageParam().data.acceptId) {
                    resPar.accept_task_id = this.getPageParam().data.acceptId;
                }
                $.comRequest('get','/task/detail',resPar,function(res){
                    _self.isPageLoading = false;
                    var acceptTask = res.data.accept_task;
                    var defaultTask = res.data.task;
                    _self.is_bind_weixin = res.data.is_bind_weixin;
                    _self.rewarde_money = res.data.rewarde_money;

                    _self.taskStatus = defaultTask.status;
                    let steps = defaultTask.custom_steps;
                    if (acceptTask) {
                        _self.acceptStatus = acceptTask.status;
                        _self.acceptId = acceptTask.id;
                        if (acceptTask.status.value == 1) {
                            _self.parseCommitTaskResidueTime(acceptTask.time_auto_release);
                        }
                        if (acceptTask.custom_steps) {
                            steps = acceptTask.custom_steps;
                        }
                    }
                    _self.taskSteps = _self.formateTaskStep(steps);
                    _self.pareTaskEndTime(defaultTask);
                    _self.parseTaskLink(defaultTask);
                    _self.taskWran = defaultTask.project.task_accept_tip;
                    _self.taskWranKey = defaultTask.project.id+'_'+defaultTask.project.task_accept_tip;

                    // tips
                    _self.acceptTaskTips();

                    _self.time_last = defaultTask.time_last;
                    _self.topTimeInit();

                },function(ero){
                    _self.isPageLoading = false;
                    $.toast(ero.message);
                },load);
            },
            topTimeInit:function(data){
                var _self = this;
                var topTimer = null;
                topTime();
                topTimer = setInterval(topTime,1000);
                function topTime(){
                    var result = _self.time_last;
                    // 剩余毫秒数
                    var d = parseInt(result/(24*60*60))//用总共的秒数除以1天的秒数
                    var h = parseInt(result/(60*60)%24);//精确小时，用去余
                    var m = parseInt(result/60%60);//剩余分钟就是用1小时等于60分钟进行趣余
                    var s = parseInt(result%60);
                    // `${d}天${h}时${m}分${s}秒`
                    var spec = d + '天' + h + '时' + m + '分' + s + '秒';
                    if(result<=0){
                        clearInterval(_self.topTimer);
                        return;
                    }
                    _self.time_lastText = spec;
                    _self.time_last--;
                }
            },
            formateTaskStep(steps) {
                var tempSteps = utils.simpleCopyObj(steps);
                var targets = this.targets;
                var commands = this.commands;
                for(var stepIndex=0;stepIndex<tempSteps.length;stepIndex++){
                    var perStep = tempSteps[stepIndex];
                    for(var sonIndex = 0;sonIndex<perStep.length;sonIndex++){
                        var sonStep = perStep[sonIndex];
                        if(sonStep.type == "Operable"){
                            var useOptions = [];
                            for(var i=0;i<targets.length;i++){
                                var curTarget = targets[i];
                                if(sonStep.target.type == curTarget.name){
                                useOptions = curTarget.operations
                                }
                            }
                            var factOptions = [];
                            for (var ot=0;ot<useOptions.length;ot++) {
                                factOptions.push(commands[useOptions[ot]]);
                            }
                            sonStep.target.content = factOptions;
                            continue;
                        }
                        if (sonStep.type == "CollectTextInfo"){
                            sonStep.value= sonStep.value ? sonStep.value : '';
                        }
                    }
                }
                return tempSteps;
            },
            pareTaskEndTime(resObj) {
                var _self = this;
                /**
                *  计算任务剩余时间
                */
                var countDown = function (time,createTimes){
                    var parseTime = ()=>{
                        var deadline_time = time*3600*1000;
                        var createTime = createTimes*1000;
                        // 现在时间毫秒数
                        var now = new Date().getTime();
                        // 剩余毫秒数
                        var residueTime =  createTime+deadline_time-now;
                        var result = parseInt(residueTime/1000);
                        var d = parseInt(result/(24*60*60))//用总共的秒数除以1天的秒数
                        var h = parseInt(result/(60*60)%24);//精确小时，用去余
                        var m = parseInt(result/60%60);//剩余分钟就是用1小时等于60分钟进行趣余
                        var s = parseInt(result%60);
                        // `${d}天${h}时${m}分${s}秒`
                        var spec = d + '天' + h + '时' + m + '分' + s + '秒';
                        if(result<=0){
                            _self.taskEndTime = {
                                isEnd: true,
                                spec: '已过期'
                            };
                            clearInterval(_self.taskDateTimer);
                            return;
                        }
                        _self.taskEndTime = {
                            isEnd: false,
                            spec: spec
                        };
                    }
                    parseTime();
                    _self.taskDateTimer = setInterval(()=>{
                        parseTime();
                    },998)
                }
                   // 无限期情况
                if (resObj.deadline_time != 0) {
                    // 任务结束时间
                    countDown(resObj.deadline_time, resObj.approval_time || resObj.time_created);
                }else{
                    this.taskEndTime = {
                        isEnd: false,
                        spec: '无限期'
                    };
                }
            },
            parseCommitTaskResidueTime: function(releaseTime) {
                clearInterval(this.commitTimer);
                var _self = this;
                const parseTime = () =>{
                        // 自动释放任务毫秒数
                    var time_auto_release = releaseTime*1000;
                    // 现在时间毫秒数
                    var now = new Date().getTime();
                    // 剩余毫秒数
                    var residueTime = time_auto_release-now;
                    var result = parseInt(residueTime/1000);
                    var d = parseInt(result/(24*60*60));//用总共的秒数除以1天的秒数
                    var h = parseInt(result/(60*60)%24);//精确小时，去余
                    var m = parseInt(result/60%60);//剩余分钟就是用1小时等于60分钟进行趣余
                    var s = parseInt(result%60);
                    _self.commitResidueText = d + '天' + h + '时' + m + '分' + s + '秒';
                    if(result<=0){
                        _self.commitResidueText =`0天0时0分0秒`;
                        clearInterval(_self.commitTimer);
                        return;
                    }
                }
                parseTime();
                this.commitTimer = setInterval(()=>{
                        parseTime()
                },998);
            },
            parseTaskLink(resObj) {
                var taskAstrict = function () {
                    var project = resObj.project;
                    if (project && project.timing_max_accepts > 0) {
                        return  project.timing_max_accepts + '次/天';
                    }
                    return '不限制';
                }
                var createDate = utils.getFormatDate(resObj.time_created);
                var createTime = createDate.year + '-' + createDate.month + '-' + createDate.day + ' ' + createDate.hour + ':' + createDate.minutes;
                var name = resObj.platform.name + ' - ' + resObj.project.name;
                var progressRatio = (resObj.total_accepts-resObj.total_surplus_accepts)/resObj.total_accepts*100;
                progressRatio = Number(progressRatio).toFixed(2);
                this.taskLink = {
                    id: resObj.id,
                    iconUrl: resObj.platform.icon.url,
                    name: name,
                    createTime: createTime,
                    progressRatio: progressRatio + '%',
                    price: $.parsePrice(resObj.amount_actual,2),
                    claim: resObj.title,
                    auditTime: resObj.limited_time_approve,
                    limit: taskAstrict(),
                    pubImg: resObj.user.avatar,
                    pubName: resObj.user.nick_name,
                    uid: resObj.user.id,
                    diffculty_level: resObj.diffculty_level,
                }
            },
              // 获取任务步骤可操作项
            getOpercommandsApi: function(load) {
                var _self = this;
                $.comRequest('get','/task/step/all_opercommands',{},function(res){
                    _self.targets = res.data.targets;
                    _self.commands = res.data.commands;
                    _self.getTaskDetailApi(load);
                },function(ero){
                    $.toast(ero.message);
                });
            },
            /**
             *  检测是否可以执行操作
             */
            checkCanHandle() {
                let isLogin = this.isLogin;
                let btnStatus = Number(this.btnStatus);
                // 按钮状态 0 未领取  1 待提交 2 已提交 3 过期
                if (!isLogin) {
                    return {
                        status: false,
                        msg: '请先登录后操作'
                    }
                }
                if ( btnStatus === 0 ) {
                    return {
                        status: false,
                        msg: '请先领取任务后再操作'
                    }
                }
                if ( btnStatus === 3 ) {
                    return {
                        status: false,
                        msg: '任务已过期'
                    }
                }
                return {
                    status: true,
                    msg: '执行'
                }
            },
            /**
             *  是否需要步骤操作
             *
             */
            isCollect(stepItem) {
                for(var i = 0;i<stepItem.length;i++) {
                    var curContent = stepItem[i];
                    if (curContent.type === 'CollectTextInfo') {
                        return true;
                    }
                    if (Array.isArray(curContent)) {
                        for (var fileIndex =0;fileIndex<curContent.length;fileIndex++) {
                            if (curContent[fileIndex].type==='CollectGallery') {
                                return true;
                            }
                        }
                    }
                }
                return false;
            },
            selectShowImg(type,arrItem,stepIndex,contIndex,arrIndex) {
                if (arrItem.uri) {
                    return arrItem.uri;
                }
                return '../../../image/task/addImg2.png';
            },
            handleImg(type,arrItem,stepIndex,contIndex,arrIndex){
                var _self = this;
                const openFile = () => {
                    if(arrItem.uri) {
                        var type = utils.isFile(arrItem.uri);
                        if (type && type === 'video') {
                            api.openVideo({
                                　　url: arrItem.uri
                            });
                        }else{
                            _self.openFile(arrItem.uri);
                        }
                    }
                }
                if (type === 'Gallery') {
                    openFile();
                    return;
                }
                let checkLink =  this.checkCanHandle();
                if (!checkLink.status){
                    $.toast(checkLink.msg);
                    return;
                }
                if (this.btnStatus == 2) {
                    openFile();
                    return;
                }
                this.curUploadLink = {
                    type: type,
                    arrItem: arrItem,
                    stepIndex: stepIndex,
                    contIndex: contIndex,
                    arrIndex: arrIndex
                }
                this.uploadImg();
            },
            // 图片预览
            openFile: function(fileUrl) {
                photoBrowser.open({
                    images: [fileUrl],
                    bgColor: '#000'
                }, function(photoRes, err) {
                    if(photoRes && photoRes.eventType=="click"){
                        photoBrowser.close();
                    }else if(photoRes && photoRes.eventType == 'longPress'){

                        var param = {title:"提示",items:["保存到系统相册"]};
                        iOSDialog.actionSheetDialog(param,function(ret, err){
                            if(ret.index == 1){
                                $.savePicture(api, function(){
                                    $.saveMediaToAlbum(fileUrl, function(){
                                        $.toast('图片已保存到系统相册');
                                    });
                                });
                            }
                        });
                    }
                });
            },
            uploadImg: function() {
                var _self = this;
                $.getPicture(api, function(ret, err){
                    if (ret.data) {
                        var img_pic1 = ret.data;
                        _self.uploadImgApi(img_pic1);
                    }
                });
            },
            uploadImgApi: function (imgUrl) {
                var _self = this;
                console.log(JSON.stringify(
                    {
                        bid: this.bid,
                        filename: 'hello',
                        type: 'jpg',
                        isaccept: _self.acceptId,
                    }
                ))
                $.comRequest('post','/file/upload',{
                    values: {
                        bid: this.bid,
                        filename: 'hello',
                        type: 'jpg',
                        isaccept: _self.acceptId,
                    },
                    files: {
                        file: imgUrl,
                    }
                },function(res){
                    var data = res.data;
                    _self.updateUploadImg(data.url,data.id);
                },function(ero){
                    $.toast(ero.message);
                },true)
            },
            updateUploadImg: function(imgUrl,fid) {
                //  type: type,
                //  arrItem: arrItem,
                //  stepIndex: stepIndex,
                //  contIndex: contIndex,
                //  arrIndex: arrIndex
                var curUploadLink = this.curUploadLink;
                var stepList = utils.simpleCopyObj(this.taskSteps);
                var curStep = stepList[curUploadLink.stepIndex];
                var curCetGery = curStep[curUploadLink.contIndex][curUploadLink.arrIndex];
                curCetGery.uri = imgUrl;
                curCetGery.fid = fid;
                this.$set(this.taskSteps,curUploadLink.stepIndex,curStep);
            },
            checkInput() {
                let checkLink =  this.checkCanHandle();
                if (!checkLink.status){
                    $.toast(checkLink.msg);
                    return;
                }
            },
            isShowContentLink(value) {
                if (this.checkCanHandle().status) {
                    return value;
                }
                return '*********';
            },
            /**
             *  文本复制
             * @param {*} text
             */
            copyText(text) {
                clipBoard.set({
                    value: text
                }, function(ret, err) {
                    if (ret) {
                        $.toast('复制成功');
                    } else {
                        $.toast('复制失败');
                    }
                });
            },
            handCoypLink(text) {
                let checkLink =  this.checkCanHandle();
                if (!checkLink.status){
                    $.toast(checkLink.msg);
                    return;
                }
                this.copyText(text);
            },
            handleSpec(btnObj,text) {
                let checkLink =  this.checkCanHandle();
                if (!checkLink.status){
                    $.toast(checkLink.msg);
                    return;
                }
                let _self = this;
                const execute = {
                    'copy': function(){
                        _self.copyText(text);
                    },
                    'useWeb': function() {
						text = text.replace(/(^\s*)|(\s*$)/g, "");

						api.openApp({
						    uri: text,
						    iosUrl: text,
						}, function(ret, err) {
						    if (!ret) {
						        api.toast({
						            msg: '打开浏览器失败',
						            duration: 2000,
						            location: 'bottom'
						        });
						    }
						});
                    },
                    'openApp': function() {
                          // 去除首尾两端空格
                        text = text.replace(/(^\s*)|(\s*$)/g, "");
                        var ext = btnObj.ext;
                        var path = ext;
                        if (ext.indexOf('$1') != -1) {
                            text = text.replace(/https:\/\//, "")
                            path = path.replace(/{\$1}/, text)
                        }
                        api.openApp({
                            uri: path,
                            iosUrl: path,
                        }, function(ret, err) {
                            if (!ret) {
                                api.toast({
                                    msg: btnObj.name+'失败',
                                    duration: 2000,
                                    location: 'bottom'
                                });
                            }
                        });
                    }
                }
                execute[btnObj.command]();
            },
            handleBtnClick: function() {
                var _this = this;
                if (!this.isLogin) {
                    $.openUrl('common/commonUser','common/login')
                    return;
                }
                var btnStatus = Number(this.btnStatus);
                // 按钮状态 0 未领取  1 待提交 2 已提交 3 过期
                if (btnStatus === 1) {
                    this.commitTaskByIdApi();
                }
                if (btnStatus === 0) {
                    if(_this.is_bind_weixin==0){
                        $.confirm({
                            title: '提示',
                            msg: "您的账号还未绑定微信号，请先绑定后再接受任务，绑定后获得" + _this.rewarde_money + "元现金",
                            buttons: ['取消','前去绑定'],
                            success:function(){
                                wxPlus.auth({}, function(authRet, authErr){
                                    if(authRet.status){
                                        wxPlus.getToken({
                                            code: authRet.code
                                        }, function(ret, err){
                                            if (ret.status) {
                                                $.ajax({
                                                    url: '/user/social/bind/weixinapp/callback',
                                                    method: 'post',
                                                    load: true,
                                                    data: {
                                                        access_token: ret.accessToken,
                                                        openid: ret.openId
                                                    },
                                                    success: function(res){
                                                        if(res.data.random_money>0){
                                                            $.alert("微信绑定成功，已为您奖励" + $.parsePrice(res.data.random_money) + "元余额奖励，请注意查收",function(){
                                                                _this.acceptTaskByIdApi();
                                                                api.sendEvent({name: 'updateMyIndexPage'});
                                                            });
                                                        }
                                                    },
                                                    error:function(res){
                                                        $.alert(res.message);
                                                    }
                                                });
                                            }else{
                                                var msg = "";
                                                if(err.code == -1){
                                                    msg = "未知错误";
                                                }else if(err.code == 1){
                                                    msg = "apiKey值为空或非法";
                                                }else if(err.code == 2){
                                                    msg = "apiSecret值为空或非法";
                                                }else if(err.code == 3){
                                                    msg = "网络超时";
                                                }
                                                $.alert(msg);
                                            }
                                        });
                                    }else{
                                        var msg = "";
                                        if(authErr.code == -1){
                                            msg = "未知错误";
                                        }else if(authErr.code == 1){
                                            msg = "您取消了授权";
                                        }else if(authErr.code == 2){
                                            msg = "您拒绝授权";
                                        }else if(authErr.code == 3){
                                            msg = "当前设备未安装微信客户端";
                                        }
                                        $.alert(msg);
                                    }
                                });
                            },
                        });
                    }else{
                        _this.acceptTaskByIdApi();
                    }
                }
            },
            commitTaskByIdApi: function() {
                var _self = this;
                if (!this.checkStepContent()) {
                    return;
                }
                var resPar = {
                    ta_id: this.acceptId,
                    bid: this.bid,
                    custom_steps:JSON.stringify(this.taskSteps)
                }
                $.comRequest('post','/task/accept/submit',resPar,function(res){
                    if(res.data.task && res.data.task.length>0){
                        api.sendEvent({
                            name: 'updateShortVideo',
                            extra: {
                                taskId:_self.taskId,//传入当前被接的短视频ID
                                videoTask:res.data.task//将新来的1-2个短视频传入到首页
                            }
                        });
                    }
                    $.toast('任务提交成功,进入审核');
                    _self.init(true);
                },function(ero){
                    $.toast(ero.message);
                },true)
            },
            checkStepContent() {
                for(let stepIndex=0; stepIndex<this.taskSteps.length; stepIndex++){
                    let curStep = this.taskSteps[stepIndex];
                    for(let ctIndex =0; ctIndex<curStep.length; ctIndex++){
                        let curCt = curStep[ctIndex];
                        let baseSpec = `第${stepIndex+1}步中第${ctIndex+1}个模块`;
                        if (Array.isArray(curCt)){
                            for (let ctImg =0; ctImg<curCt.length; ctImg++) {
                                let curImg = curCt[ctImg];
                                if(curImg.type === "CollectGallery" && utils.isNullStr(curImg.uri || '')) {
                                    $.toast(baseSpec  + '需要上传相关截图');
                                    return false;
                                }
                            }
                        }
                        if (curCt.type === "Description") {
                            if (utils.isNullStr(curCt.value)) {
                                $.toast(baseSpec + '需要填写介绍文字!');
                                return false;
                            }
                        }
                        if (curCt.type === 'CollectTextInfo') {
                            if (utils.isNullStr(curCt.value)) {
                                $.toast(baseSpec + '需要填写'+ curCt.placeholder);
                                return false;
                            }
                            this.handleTextInputCache('set',curCt.value);
                        }
                    }
                }
                return true;
            },
            handleTextInputCache(type,value) {
                if (type === 'set') {
                    var textList =  $.handleLocalStorage.get('textList');
                    textList = textList || [];
                    if (textList.indexOf(utils.trim(value)) != -1) {
                        return;
                    }
                    textList.push(value);
                    if (textList.length > 9) {
                        textList = textList.slice(0,8);
                    }
                    $.handleLocalStorage.set('textList',textList);
                }else{
                    var originList =   $.handleLocalStorage.get('textList')
                    originList = utils.arrDistinct(originList);
                    return  originList;
                }
            },
            // 领取任务
            acceptTaskByIdApi: function() {
                var _self = this;
                $.comRequest('post','/task/accept',{
                    task_id: this.taskId
                },function(res){
                    $.toast('领取成功');
                    api.sendEvent({
                        name:'updateHomeTaskItemIsAbleAccept',
                        extra: {
                            taskId: _self.taskId,
                            isAbleAccept: 0
                        }
                    });
                    _self.init(true);
                },function(ero){
					api.hideProgress();
                    $.toast(ero.message);
                },true,0);
            },
            giveUpTaskApi() {
                var _self = this;
				$.confirm({
				    title: '提示',
				    msg: '确定放弃该任务?',
				    buttons: ['取消','确定'],
				    success:function(){
				        $.comRequest('post','/task/accept/release',{
				                ta_id: _self.acceptId
				            },
				            function(){
				                $.toast('放弃任务成功');
				                api.sendEvent({
				                    name:'updateHomeTaskItemIsAbleAccept',
				                    extra: {
				                        taskId: _self.taskId,
				                        isAbleAccept: 1
				                    }
				                });
				                _self.init(true);
				            },
				            function(ero){
								api.hideProgress();
				                $.toast(ero.message);
				            },
							true,0
				        );
				    }
				});
            },
            selectComplain: function () {
                var _self = this;
                var slotObj = {
                    1: function () {
                        _self.commitComplainApi('任务内容违规');
                    },
                    2: function () {
                        _self.commitComplainApi('已过期,无法进行任务');
                    },
                    3: function () {
						$.prompt({
						    title:"提示",
						    msg:"请输入投诉内容",
						    buttons:['取消','确定'],
						    success:function(res){
						        if (utils.isNullStr(res)) {
						            $.toast('投诉不能为空哦');
						            return;
						        }
						        _self.commitComplainApi(res);
						    }
						});
                    }
                }
                var param = {title:"请选择",items: [
                    '任务内容违规',
                    '已过期,无法进行任务',
                    '其他'
                ]};
                iOSDialog.actionSheetDialog(param,function(ret, err){
                    if (ret.index && ret.index > 0) {
                        slotObj[ret.index]();
                    }
                });
            },
            selectTextCache(stepIndex,contIndex) {
                var textList = this.handleTextInputCache('get');
                if(!textList[0]) {
                    return;
                }
                var param = {
                    title: "请选择",
                    items: textList
                };
                var taskSteps = utils.simpleCopyObj(this.taskSteps);
                var curStep = taskSteps[stepIndex];
                var curCont = curStep[contIndex];
                var _self = this;
                iOSDialog.actionSheetDialog(param,function(ret, err){
                    if (ret.index && ret.index > 0) {
                        curCont.value = textList[ret.index - 1];
                        curStep[contIndex] = curCont;
                        _self.$set(_self.taskSteps,stepIndex,curStep);
                    }
                });
            },
            commitComplainApi: function (text) {
                var taskId = this.taskLink.id;
                $.comRequest('post','/task/accept/report',{
                        remark: text,
                        ta_id: taskId
                    },
                    function(){
                        $.toast('投诉成功,感谢您的反馈');
                    },
                    function(ero){
                        $.toast(ero.message);
                    }
                );
            },
            lookOverWarn: function () {
                var cacheKey = "acceptTaskDetailTips";
                var key = md5(this.taskWranKey).substr(0, 10);
                var caches = JSON.parse($.prefs.getItem(cacheKey)) || {};
                var msg = this.taskWran;
                $.confirm({
                    title: '提示',
                    msg: this.taskWran,
                    buttons: ['我知道了', '不再提示'],
                    success: function(){
                        caches[key] = 1;
                        $.prefs.setItem(cacheKey, JSON.stringify(caches));
                    },
                });
            },
            acceptTaskTips: function(){
                var cacheKey = "acceptTaskDetailTips";
                var key = md5(this.taskWranKey).substr(0, 10);
                var caches = JSON.parse($.prefs.getItem(cacheKey)) || {};
                var msg = this.taskWran;
                if( !msg || caches[key] == 1){
                    return false;
                }
                this.lookOverWarn();
            },
        },
    });
}
