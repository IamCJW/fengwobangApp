/**
 * 工具函数, 不可依赖外部任何包，与任何项目业务逻辑无任何关系
 */
(function(window){
var TOOLS = {
    isArray: function(o){
        return Object.prototype.toString.call(o) === '[object Array]';
    },
    isObject: function(o){
        return Object.prototype.toString.call(o) === '[object Object]';
    },
    isFile: function(url){
        if(!url) {
            return false;
        }
        let that = this;
        //获取最后一个.的位置
        let index = url.lastIndexOf('.');
        // 获取后缀
        let ext = url.substr(index+1);
        // console.log(ext);

        function isAssetTypeAnImage (ext){
            return ['png', 'jpg', 'jpeg', 'gif'].indexOf(ext.toLowerCase()) !== -1;
        }
        function isAssetTypeAnVideo (ext){
            return ['mov', 'mp4'].indexOf(ext.toLowerCase()) !== -1;
        }
        if(isAssetTypeAnImage(ext)){
            return "img"
        }else if(isAssetTypeAnVideo(ext)){
            return 'video'
        }else{
            return false
        }
    },
    isNullStr: function(str) {
        if(!str) {
            return true;
        }
        // 兼容旧浏览器
        if(str.trim) {
            str = str.trim();
        }else{
            str = str.replace(/(^\s*)|(\s*$)/g, "");
        }
        if (str === '' || str === 'null' || str === 'undefined') {
            return true;
        }
        return false;
    },
    // 简易的对象拷贝
    simpleCopyObj: function(stayCopy) {
        return JSON.parse(JSON.stringify(stayCopy));
    },

    // 时间格式化 tsToDateFormat(Date.now(), 'yyyy/MM/dd hh:mm:ss');
    tsToDateFormat: function (ts, fmt) {
        fmt = fmt || 'yyyy-MM-dd hh:mm:ss';
        if (ts.toString().length === 10) {
            ts *= 1000;
        }
        var D = new Date(ts);
        var o = {
            "M+": D.getMonth() + 1, //月份
            "d+": D.getDate(), //日
            "h+": D.getHours(), //小时
            "m+": D.getMinutes(), //分
            "s+": D.getSeconds(), //秒
            "q+": Math.floor((D.getMonth() + 3) / 3), //季度
            "S": D.getMilliseconds() //毫秒
        };
        if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (D.getFullYear() + "").substr(4 - RegExp.$1.length));
        for (var k in o)
            if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
        return fmt;
    },

    /**
     *  获取格式化时间 
     *  @param { String } milliseconds 毫秒数
     *  @return dateObj 年,月,日,时,分,秒
     */
    getFormatDate: function (milliseconds) {
        milliseconds = String(milliseconds).length > 10 ? Number(milliseconds) : Number(milliseconds) * 1000;
        var curDate = new Date(milliseconds);
        var year = curDate.getFullYear();
        var month = curDate.getMonth() + 1;
        var day = curDate.getDate();
        var hour = curDate.getHours();
        var minutes = curDate.getMinutes();
        var seconds = curDate.getSeconds();
        return {
            year: year,
            month: month,
            day: day,
            hour: hour > 9 ? hour : '0' + hour,
            minutes: minutes > 9 ? minutes : '0' + minutes,
            seconds: seconds > 9 ? seconds : '0' + seconds
        }
    },

    // 时间距离格式化
    // x小时, x分钟
    timeAway: function(ts){
        var now = Date.now();
        ts = ts.toString().length > 10 ? ts : ts * 1000;
        diff = (now - ts) / 1000;
        if(diff >= 86400){
            return TOOLS.tsToDateFormat(ts, 'yyyy/MM/dd hh:mm');
        }else if(diff >= 3600){
            return Math.floor(diff / 60 / 60) + '小时前';
        }else if(diff >= 60){
            return Math.floor(diff / 60) + '分钟前';
        }else{
            return Math.floor(diff) + '秒前'
        }
    },

    //将base64转换为文件对象
    dataURLtoFile: function (dataurl, filename) {
        var arr = dataurl.split(',');
        var mime = arr[0].match(/:(.*?);/)[1];
        var bstr = atob(arr[1]);
        var n = bstr.length; 
        var u8arr = new Uint8Array(n);
        while(n--){
            u8arr[n] = bstr.charCodeAt(n);
        }
        //转换成file对象
        return new File([u8arr], filename, {type:mime});
        //转换成成blob对象
        //return new Blob([u8arr],{type:mime});
    },
    trim:  function (str){ //删除左右两端的空格
        return str.replace(/(^\s*)|(\s*$)/g, "");
    },
    arrDistinct: function(stayArr) {
        let arr = utils.simpleCopyObj(stayArr);
        for (let i=0, len=arr.length; i<len; i++) {
            for (let j=i+1; j<len; j++) {
                if (arr[i] == arr[j]) {
                    arr.splice(j, 1);
                    // splice 会改变数组长度，所以要将数组长度 len 和下标 j 减一
                    len--;
                    j--;
                }
            }
        }
        return arr
    },
    objectAssgin: function assign(target) {
        if (target == null) {
            throw new TypeError('Cannot convert undefined or null to object');
        }
  
        let to = Object(target);

        
        for (var index = 1; index < arguments.length; index++) {
            var nextSource = arguments[index];
            
            if (nextSource != null) { // Skip over if undefined or null
                for (let nextKey in nextSource) {
                    // Avoid bugs when hasOwnProperty is shadowed
                    if (Object.prototype.hasOwnProperty.call(nextSource, nextKey)) {
                        to[nextKey] = nextSource[nextKey];
                    }
                }
            }
        }
        return to;
      },
    objectValues: function(obj){
        
        var val=[],key;
        for (key in obj) {
            if (Object.prototype.hasOwnProperty.call(obj,key)) {
                val.push(obj[key]);
            }
        }
        return val;
    },
};

window.utils = TOOLS;
})(window);