/**
 * vue touch
 * vue v2.0适用
 * <div v-tap={ methods : 方法 , params: 参数} ></div>
 */

function vueTouch(el,binding,type){
	var _this = this;
	this.obj = el;
	this.binding = binding;
	this.touchType = type;
	this.vueTouches = {x:0, y:0};
	this.vueMoves = true;
	this.vueLeave = true;
	this.vueCallBack = typeof(binding.value) == "object" ? binding.value.methods : binding.value;
	this.obj.addEventListener("touchstart",function(e){
		_this.start(e);
	},false);
	this.obj.addEventListener("touchend",function(e){
		_this.end(e);
	},false);
	this.obj.addEventListener("touchmove",function(e){
		_this.move(e);
	},false);
};
vueTouch.prototype = {

	// 监听touchstart事件
	start: function(e){
		this.vueMoves=true;
		this.vueLeave=true;
		this.longTouch=true;
		this.vueTouches={x:e.changedTouches[0].pageX,y:e.changedTouches[0].pageY};
		this.timer=setTimeout(function(){
			if(this.vueLeave&&this.vueMoves){
				this.touchType=="longtap"&&this.vueCallBack(this.binding.value.params,e);
				this.longTouch=false;
			};
		}.bind(this),1000);
	},

	// 监听touchend事件
	end: function(e){

		// 计算移动的位移差
		var disX = e.changedTouches[0].pageX - this.vueTouches.x;
		var disY = e.changedTouches[0].pageY - this.vueTouches.y;
		this.timer = clearTimeout(this.timer);

		// 当横向位移大于10，纵向位移大于100，则判定为滑动事件
		if(Math.abs(disX) > 10 || Math.abs(disY) > 100){

			// 若为滑动事件则返回
			this.touchType == "swipe" && this.vueCallBack(this.binding.value.params,e);

			// 判断是横向滑动还是纵向滑动
			if(Math.abs(disX) > Math.abs(disY)){

				// 右滑
				if(disX > 10){
					this.touchType == "swiperight" && this.vueCallBack(this.binding.value.params,e);
				};

				// 左滑
				if(disX < -10){
					this.touchType == "swipeleft" && this.vueCallBack(this.binding.value.params,e);
				};
			}else{

				// 下滑
				if(disY > 10){
					this.touchType == "swipedown" && this.vueCallBack(this.binding.value.params,e);
				};

				// 上滑
				if(disY < -10){
					this.touchType == "swipeup" && this.vueCallBack(this.binding.value.params,e);
				};  
			};
		}else{
			if(this.longTouch && this.vueMoves){
				this.touchType == "tap" && this.vueCallBack(this.binding.value.params,e);
				this.vueLeave = false
			};
		};
	},

	// 监听touchmove事件
	move: function(e){
		this.vueMoves = false;
	}
};

// 点击事件
Vue.directive("tap",{
	bind: function(el,binding){
		new vueTouch(el,binding,"tap");
	}
});

// 滑动事件
Vue.directive("swipe",{
	bind: function(el,binding){
		new vueTouch(el,binding,"swipe");
	}
});

// 左滑事件
Vue.directive("swipeleft",{
	bind: function(el,binding){
		new vueTouch(el,binding,"swipeleft");
	}
});

// 右滑事件
Vue.directive("swiperight",{
	bind: function(el,binding){
		new vueTouch(el,binding,"swiperight");
	}
});

// 下滑事件
Vue.directive("swipedown",{
	bind: function(el,binding){
		new vueTouch(el,binding,"swipedown");
	}
});

// 上滑事件
Vue.directive("swipeup",{
	bind: function(el,binding){
		new vueTouch(el,binding,"swipeup");
	}
});

// 长按事件
Vue.directive("longtap",{
	bind: function(el,binding){
		new vueTouch(el,binding,"longtap");
	}
});