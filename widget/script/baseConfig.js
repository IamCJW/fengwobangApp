/**
 * 全局公共配置
 */
var CONFIG = {

    // 项目环境  dev: 开发,  prod: 线上
    env: 'dev',
    updateCheck:true,
    error:true,
    httpConfig: {
        host:{ //接口地址
            dev: 'http://94.191.52.70:8080/',
            prod: 'https://api.fengwobang.com',
        },
		web:{ //H5网页地址，用于嵌套页面
			dev:'http://94.191.52.70:8090/',
			prod:'https://h5.fengwobang.com',
		},
		oss:"https://img.fengwobang.com", //静态文件OSS云储存地址
    },

	//无需登录白名单内容页
	noMustLogin:[
		'index/search',
		'common/filter',
		'common/login',
		'common/reg',
		'common/findPass',
		'common/bindMobile',
		'task/detail',
        'my/service',
	],

    shortVideoId:[3],

    // 金额比例
    PRICE_RADIO: 10000,

    // 常量
    constants: {

        // 文件上传类型
        TASK: 'Task',
        AVATAR: 'Avatar',

        // 邀请分享信息
        SHARE_WX_TITLE: '蜂窝帮 - 让您的时间更有价值',
        SHARE_WX_DESCRIPTION: '利用空闲任务轻松赚钱，赚得佣金可随时提现。',
        SHARE_WX_IMG: '',
        SHARE_QQ_TITLE: '蜂窝帮 - 让您的时间更有价值',
        SHARE_QQ_DESCRIPTION: '利用空闲任务轻松赚钱，赚得佣金可随时提现。',
        SHARE_QQ_IMG: '',

        // 任务详情分享
        SHARE_WX_TITLE_TASK: '就差你了，来蜂窝帮和我一起领红包',
        SHARE_WX_DESCRIPTION_TASK: '快来和我一起完成任务，红包天天领',
        SHARE_WX_IMG_TASK: '',
        SHARE_QQ_TITLE_TASK: '就差你了，来蜂窝帮和我一起领红包',
        SHARE_QQ_DESCRIPTION_TASK: '快来和我一起完成任务，红包天天领',
        SHARE_QQ_IMG_TASK: '',

    },

    // 缓存key
    cacheKeys: {
        USERINFO: 'userSessions',
    },

    // 派单状态
    sendStatus: [
        {id:1,title:'全部',status:''},
        {id:2,title:'待发布',status:1},
        {id:3,title:'进行中',status:2},
        {id:4,title:'已完成',status:3},
        {id:5,title:'已关闭',status:4},
        {id:6,title:'清退中',status:5},
        // {id:7,title:'暂停中',status:6},
    ],

    // 接单状态
    acceptStatus: [
        {id:1,title:'全部',status:''},
        {id:2,title:'进行中',status:1},
        {id:4,title:'待审核',status:2},
        {id:3,title:'已通过',status:3},
        {id:5,title:'仲裁中',status:4},
        {id:6,title:'未通过',status:5},
        {id:7,title:'已关闭',status:6},
    ],

    // 聊天注册账号密码
    chatPass: '123455',

};
