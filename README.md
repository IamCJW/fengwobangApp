# 蜂窝帮APP

#### 使用说明

1.  develop： 开发分支，测试分支
2.  release： 生产发版分支
3.  feature： 特性开发分支

平时开发测试在 `develop` 分支上进行，由于没用测试环境，开发测试都在develop分支上进行。
特性开发在feature上，新特性完成后，如果需要测试并上线，先合并到develop分支上测试。

## 发版流程：
在 `develop` 分支代码测试没问题后，修改配置环境为：`prod`，再将分支切换为 `release`，合并`develop`后再提交发版。
